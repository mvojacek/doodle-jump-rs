# Doodlejump-rs

Classic arcade game game written in [Rust](https://www.rust-lang.org/) using the [Amethyst engine](https://amethyst.rs)

## Build and run the project

### Install the rust toolchain

Follow instructions on [rustup.rs](https://rustup.rs)

### Optimized release build

*This may take 30 minutes or more.* A sizable compilation cache (can be larger than 10G) will be created in `./target/`,
speeding up subsequent builds (with the same configuration, a `debug` build would take another 30mins, taking up even more
space in `./target`).

```bash
cargo run --release --no-default-features --features vulkan,no-slow-safety-checks
```

To build on Mac, change `vulkan` to `metal` in the above.  
If using OSX and Metal you will require full XCode installed from the Appstore in order to compile metal shaders.
After install you may be required to run this command `sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer` [reference gfx-rs issue](https://github.com/gfx-rs/gfx/issues/2472)

### Release build with inspector

```bash
cargo run --release
```

### Debug build with inspector

```bash
cargo run
```

## Distribution

Built binaries are available in `./target/{release,debug}/doodlejump[.exe]`. They expect to be run in a directory
containing the `./resources` folder, which must be shipped alongside it in a distribution.