use amethyst::{
    core::bundle::SystemBundle,
    ecs::prelude::{DispatcherBuilder, World},
    error::Error,
    assets::AssetStorage,
};

use crate::system::{
    ResizeCameraSystemDesc,
    FollowMouseSystemDesc,
    MovementSystemDesc,
    JumperCollisionSystemDesc,
    PlatformPlacerSystemDesc,
    CameraScrollerSystemDesc,
};
use amethyst::prelude::WorldExt;
use amethyst::core::SystemDesc;
use amethyst_inspector::{inspector, InspectorHierarchy};
use amethyst::core::{Named, Transform};
use amethyst::renderer::SpriteRender;
use amethyst::assets::PrefabLoaderSystemDesc;
use std::fmt::Debug;
use amethyst::core::Parent;
use amethyst::input::BindingTypes;
use serde::export::PhantomData;
use crate::prefab::{JumperPrefab, PlatformPrefab, TextPrefab};
use amethyst::renderer::debug_drawing::DebugLines;
use std::path::Path;

#[derive(Default)]
pub struct BaseBundle<T: BindingTypes> {
    _marker: PhantomData<T>,
}

impl<'a, 'b, T: BindingTypes> SystemBundle<'a, 'b> for BaseBundle<T> {
    fn build(self, world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<(), Error> {
        //builder.add(ResizeCameraSystemDesc.build(world), "resize_camera_system", &[]);

        builder.add(PrefabLoaderSystemDesc::<JumperPrefab>::default().build(world), "prefab_loader_system_jumper", &[]);
        builder.add(PrefabLoaderSystemDesc::<PlatformPrefab>::default().build(world), "prefab_loader_system_platform", &[]);
        builder.add(PrefabLoaderSystemDesc::<TextPrefab>::default().build(world), "prefab_loader_system_text", &[]);

        world.insert(DebugLines::new());

        Ok(())
    }
}
