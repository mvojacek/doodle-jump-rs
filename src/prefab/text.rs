use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use amethyst::renderer::sprite::prefab::SpriteScenePrefab;
use amethyst::assets::ProgressCounter;
use amethyst::core::Named;
use std::borrow::Cow;
use amethyst::ui::{UiText, UiTransform, Anchor, LineMode, FontAsset};
use std::marker::PhantomData;
use std::ops::Deref;
use crate::component::UiTextTemplate;

#[derive(Debug, Clone, Serialize, Deserialize, PrefabData)]
pub struct TextPrefab {
    pub name: Named,
    pub text: UiTextPrefabComp,
    pub transform: UiTransformPrefabComp,
    pub template: Option<UiTextTemplate>,
}

#[derive(Clone, Serialize, Deserialize, Derivative)]
#[derivative(Default, Debug)]
#[serde(default)]
#[serde(rename = "UiText")]
pub struct UiTextPrefabComp<T: AsFont = Handle<FontAsset>> {
    pub text: String,
    #[derivative(Default(value = "50."))]
    pub font_size: f32,
    #[derivative(Default(value = "[0., 0., 0., 1.]"))]
    pub color: [f32; 4],
    pub password: bool,
    #[derivative(Default(value = "LineMode::Single"))]
    pub line_mode: LineMode,
    #[derivative(Default(value = "Anchor::Middle"))]
    pub align: Anchor,
    #[serde(skip)]
    pub _marker: PhantomData<T>,
}

pub trait AsFont {
    fn as_font(&self) -> Handle<FontAsset>;
}

impl AsFont for Handle<FontAsset> {
    fn as_font(&self) -> Handle<FontAsset> {
        (*self).clone()
    }
}

impl<'a, T: AsFont> PrefabData<'a> for UiTextPrefabComp<T>
    where T: Send + Sync + 'static {
    type SystemData = (WriteStorage<'a, UiText>, ReadExpect<'a, T>);
    type Result = ();

    fn add_to_entity(
        &self,
        entity: Entity,
        (storage, font): &mut Self::SystemData,
        _: &[Entity],
        _: &[Entity],
    ) -> Result<(), Error> {
        let mut component = UiText::new(
            font.as_font(),
            self.text.clone(),
            self.color,
            self.font_size,
        );
        component.password = self.password;
        component.line_mode = self.line_mode.clone();
        component.align = self.align.clone();

        storage.insert(entity, component).map(|_| ())?;
        Ok(())
    }
}

#[derive(Clone, Serialize, Deserialize, Derivative)]
#[derivative(Debug)]
#[serde(rename = "UiTransform")]
pub struct UiTransformPrefabComp {
    pub id: String,
    #[serde(default = "serde_ui_transform::anchor")]
    pub anchor: Anchor,
    #[serde(default = "serde_ui_transform::pivot")]
    pub pivot: Anchor,
    pub translation: [f32; 3],
    pub size: [f32; 2],
}

mod serde_ui_transform {
    use amethyst::ui::Anchor;

    pub fn anchor() -> Anchor { Anchor::Middle }

    pub fn pivot() -> Anchor { Anchor::Middle }
}

impl<'a> PrefabData<'a> for UiTransformPrefabComp {
    type SystemData = WriteStorage<'a, UiTransform>;
    type Result = ();

    fn add_to_entity(
        &self,
        entity: Entity,
        storage: &mut Self::SystemData,
        _: &[Entity],
        _: &[Entity],
    ) -> Result<(), Error> {
        let component = UiTransform::new(
            self.id.clone(),
            self.anchor.clone(),
            self.pivot.clone(),
            self.translation[0],
            self.translation[1],
            self.translation[2],
            self.size[0],
            self.size[1],
        );
        storage.insert(entity, component).map(|_| ())?;
        Ok(())
    }
}
