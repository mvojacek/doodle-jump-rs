use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use amethyst::renderer::sprite::prefab::SpriteScenePrefab;
use amethyst::assets::ProgressCounter;
use amethyst::core::Named;
use std::borrow::Cow;
use ron::ser::PrettyConfig;
use crate::component::{Jumper, Velocity2, Momentum2, Platform, FollowMouse};
use crate::component::bounding_box::BoundingBox;

pub mod text;

pub use text::TextPrefab;

#[derive(Debug, Clone, Serialize, Deserialize, PrefabData)]
pub struct JumperPrefab {
    name: Named,
    sprite_scene: SpriteScenePrefab,
    jumper: Jumper,
    velocity: Velocity2,
    momentum: Momentum2,
    bb: BoundingBox,
    follow_mouse: Option<FollowMouse>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PrefabData)]
pub struct PlatformPrefab {
    name: Named,
    sprite_scene: SpriteScenePrefab,
    platform: Platform,
    bb: BoundingBox,
}

