use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::Transform,
    input::{get_key, is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    window::ScreenDimensions,
    ecs::prelude::{Entity, WriteStorage, ReadStorage, ReadExpect, Join},
    ui::{UiTransform, UiText, FontAsset, Anchor},
    input::StringBindings,
};
use log::info;
use amethyst::assets::PrefabLoader;
use amethyst::assets::ProgressCounter;
use amethyst::assets::Completion;
use amethyst::assets::RonFormat;
use amethyst::assets::Handle;
use amethyst::assets::Prefab;
use crate::prefab::{JumperPrefab, PlatformPrefab};
use std::path::Path;
use crate::config::PlatformPlacerConfig;
use crate::state::InGameState;
use crate::state::in_game::Score;
use itertools::Itertools;
use crate::state::initial::TextPrefabs;
use crate::component::UiTextTemplate;
use dynfmt::{Format, SimpleCurlyFormat};

#[derive(new, Default)]
pub struct GameOverState {
    #[new(default)]
    text: Option<Entity>,
    #[new(default)]
    score_set: bool,
}

impl SimpleState for GameOverState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        self.position_camera(world);
        self.text = Some(self.create_game_over_text(world));
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_entity(self.text.take().unwrap()).unwrap()
    }

    fn handle_event(&mut self, data: StateData<'_, GameData<'_, '_>>, event: StateEvent<StringBindings>) -> SimpleTrans {
        if let StateEvent::Window(event) = event {
            if let Some((key, state)) = get_key(&event) {
                info!("handling key event: ({:?}, {:?})", key, state);
                if key == VirtualKeyCode::R {
                    return Trans::Switch(Box::new(InGameState::from_world(data.world)));
                }
            }
        }

        Trans::None
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        let world = &data.world;

        if !self.score_set {
            if let Some(entity) = self.text {
                if let Some(text) = world.write_storage::<UiText>().get_mut(entity) {
                    let score = world.read_resource::<Score>().score_text();

                    let formatted = if let Some(template) = world.read_storage::<UiTextTemplate>().get(entity) {
                        SimpleCurlyFormat.format(&*template.template, &[score]).unwrap().into_owned()
                    } else {
                        score
                    };

                    text.text = formatted;
                    self.score_set = true;
                }
            }
        }

        Trans::None
    }
}

impl GameOverState {
    pub fn position_camera(&mut self, w: &mut World) {
        w.exec(|(mut transforms, cameras, screen): (WriteStorage<Transform>, ReadStorage<Camera>, ReadExpect<ScreenDimensions>)| {
            let (transform, _) = (&mut transforms, &cameras).join().exactly_one().map_err(|_| ()).unwrap();
            *transform = Default::default();
            transform.set_translation_xyz(screen.width() / 2., screen.height() / 2., 1.);
        });
    }

    fn create_game_over_text(&mut self, w: &mut World) -> Entity {
        let prefab = (*w.read_resource::<TextPrefabs>()).game_over.clone();

        w.create_entity()
            .with(prefab)
            .build()
    }
}
