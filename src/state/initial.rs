use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::Transform,
    input::{get_key, is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ecs::prelude::{Write},
    window::ScreenDimensions,
    ui::{TtfFormat, FontAsset},
};
use log::info;
use amethyst::assets::PrefabLoader;
use amethyst::assets::ProgressCounter;
use amethyst::assets::Completion;
use amethyst::assets::RonFormat;
use amethyst::assets::Handle;
use amethyst::assets::Prefab;
use crate::prefab::{JumperPrefab, PlatformPrefab, TextPrefab};
use std::path::Path;
use crate::config::PlatformPlacerConfig;
use crate::util::multi_progress_counter::MultiProgressCounter;

#[derive(new)]
pub struct InitialState<A: AsRef<Path>> {
    platform_placer_config_path: A,
    #[new(default)]
    progress: MultiProgressCounter,
}

impl<A: AsRef<Path>> SimpleState for InitialState<A> {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let dimensions = (*world.read_resource::<ScreenDimensions>()).clone();

        self.init_camera(world, &dimensions);

        world.insert(PlatformPlacerConfig::load(&self.platform_placer_config_path));

        self.load_jumper(world);
        self.load_platform(world);
        self.load_default_font(world);
        self.load_texts(world);
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if self.is_loaded() {
            return SimpleTrans::Switch(Box::new(super::InGameState::from_world(data.world)));
        }
        SimpleTrans::None
    }
}

impl<A: AsRef<Path>> InitialState<A> {
    pub fn is_loaded(&self) -> bool {
        match self.progress.complete() {
            Completion::Complete => true,
            Completion::Loading => false,
            Completion::Failed => panic!("Loading failed: {:?}", self.progress.errors())
        }
    }

    fn init_camera(&self, world: &mut World, dimensions: &ScreenDimensions) {
        let mut transform = Transform::default();
        transform.set_translation_xyz(dimensions.width() * 0.5, 300., 1.);

        world
            .create_entity()
            .named("camera")
            .with(Camera::standard_2d(dimensions.width(), dimensions.height()))
            .with(transform)
            .build();
    }

    fn load_jumper(&mut self, world: &mut World) {
        let handle = world.exec(|loader: PrefabLoader<'_, JumperPrefab>| {
            loader.load("prefabs/jumper.ron", RonFormat, self.progress.next())
        });
        world.insert(handle);
    }

    fn load_platform(&mut self, world: &mut World) {
        let handle = world.exec(|loader: PrefabLoader<'_, PlatformPrefab>| {
            loader.load("prefabs/platform.ron", RonFormat, self.progress.next())
        });
        world.insert(handle);
    }

    fn load_default_font(&mut self, world: &mut World) {
        let handle = world.read_resource::<Loader>().load(
            "fonts/square.ttf",
            TtfFormat,
            self.progress.next(),
            &world.read_resource::<AssetStorage<FontAsset>>(),
        );
        world.insert(handle);
    }

    fn load_texts(&mut self, world: &mut World) {
        let prefabs = world.exec(|loader: PrefabLoader<TextPrefab>| {
            let in_game_score = loader.load("prefabs/text/in_game_score.ron", RonFormat, self.progress.next());
            let game_over = loader.load("prefabs/text/game_over.ron", RonFormat, self.progress.next());
            TextPrefabs { in_game_score, game_over }
        });
        world.insert(prefabs);
    }
}

pub struct TextPrefabs {
    pub in_game_score: Handle<Prefab<TextPrefab>>,
    pub game_over: Handle<Prefab<TextPrefab>>,
}

