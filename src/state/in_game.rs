use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::Transform,
    input::{get_key, is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ecs::prelude::{Read, ReadStorage, WriteStorage, Entities, Write, ReadExpect, Entity, WriteExpect},
    window::ScreenDimensions,
    shred::{Dispatcher, DispatcherBuilder},
    shrev::ReaderId, shrev::EventChannel,
    winit::{MouseButton, ModifiersState, ElementState},
    core::ArcThreadPool,
};
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::ecs::Component;
use amethyst::ecs::join::Join;
use amethyst::core::math::base::{Vector2, Vector3};

use log::info;
use amethyst::assets::Handle;
use crate::util::Direction2;
use std::borrow::Cow;
use amethyst::assets::PrefabLoader;
use amethyst::assets::ProgressCounter;
use amethyst::assets::RonFormat;
use once_cell::unsync::Lazy;
use amethyst::assets::Prefab;
use crate::prefab::{JumperPrefab, PlatformPrefab};
use std::ops::Deref;
use crate::component::{Platform, Jumper, Velocity2, UiTextTemplate, BoundingBox};
use itertools::Itertools;
use crate::system::{
    ResizeCameraSystemDesc,
    FollowMouseSystemDesc,
    MovementSystemDesc,
    JumperCollisionSystemDesc,
    PlatformPlacerSystemDesc,
    CameraScrollerSystemDesc,
};
use crate::config::PlatformPlacerConfig;

use amethyst::ui::{
    UiText, Anchor, UiTransform, FontAsset,
};
use crate::state::GameOverState;
use crate::state::initial::TextPrefabs;
use dynfmt::{SimpleCurlyFormat, Format};

pub struct InGameState<'a, 'b> {
    pub prefab_jumper: Handle<Prefab<JumperPrefab>>,
    pub prefab_platform: Handle<Prefab<PlatformPrefab>>,
    pub platform_placer_config: PlatformPlacerConfig,
    pub font: Handle<FontAsset>,

    pub dispatcher: Option<Dispatcher<'a, 'b>>,
}

impl InGameState<'_, '_> {
    pub fn new(jumper: Handle<Prefab<JumperPrefab>>,
               platform: Handle<Prefab<PlatformPrefab>>,
               platform_config: PlatformPlacerConfig,
               font: Handle<FontAsset>) -> Self {
        InGameState {
            prefab_jumper: jumper,
            prefab_platform: platform,
            platform_placer_config: platform_config,
            font,
            dispatcher: None,
        }
    }

    pub fn from_world(w: &mut World) -> Self {
        InGameState::new(
            (*w.read_resource::<Handle<Prefab<JumperPrefab>>>()).clone(),
            (*w.read_resource::<Handle<Prefab<PlatformPrefab>>>()).clone(),
            (*w.read_resource::<PlatformPlacerConfig>()).clone(),
            (*w.read_resource::<Handle<FontAsset>>()).clone(),
        )
    }

    pub fn duplicate<'aa, 'bb>(&self) -> InGameState<'aa, 'bb> {
        InGameState::new(
            self.prefab_jumper.clone(),
            self.prefab_platform.clone(),
            self.platform_placer_config.clone(),
            self.font.clone(),
        )
    }
}

impl<'a, 'b> SimpleState for InGameState<'a, 'b> {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        let score_entity = self.create_score_text(world);
        world.insert(Score::new(Some(score_entity)));

        world.insert(self.prefab_platform.clone());

        self.create_base_platform(world);
        self.create_jumper(world);

        self.position_camera(world);

        self.dispatcher = Some(self.create_dispatcher(world));
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let w = data.world;

        w.exec(|(entities, jumpers, platforms, mut score): (Entities, ReadStorage<Jumper>, ReadStorage<Platform>, WriteExpect<Score>)| {
            for (entity, _) in (&entities, &jumpers).join() {
                entities.delete(entity).unwrap();
            }
            for (entity, _) in (&entities, &platforms).join() {
                entities.delete(entity).unwrap();
            }
            if let Some(e) = score.text.take() {
                entities.delete(e).unwrap()
            }
        });
    }


    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: StateEvent,
    ) -> SimpleTrans {
        let _world = data.world;

        if let StateEvent::Window(event) = &event {
            // Check if the window should be closed
            if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
                return Trans::Quit;
            }

            // Listen to any key events
            if let Some((key, state)) = get_key(&event) {
                info!("handling key event: ({:?}, {:?})", key, state);
                if key == VirtualKeyCode::R {
                    return Trans::Switch(Box::new(self.duplicate()));
                }
            }
        }

        // Keep going
        Trans::None
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if let Some(dispatcher) = self.dispatcher.as_mut() {
            dispatcher.dispatch(data.world);
        }

        self.update_loss_condition(data.world)
    }
}

impl InGameState<'_, '_> {
    pub fn update_loss_condition(&mut self, w: &mut World) -> SimpleTrans {
        w.exec(|(jumpers, velocities, transforms, cameras):
                (ReadStorage<Jumper>, ReadStorage<Velocity2>, ReadStorage<Transform>, ReadStorage<Camera>)| {
            let death_threshold_y: Lazy<f32, _> = Lazy::new(|| {
                let (cam_transform, cam) = (&transforms, &cameras).join().next().unwrap();

                let cam_bottom = cam.projection().as_orthographic().unwrap().bottom();
                let global_center_y = cam_transform.translation().y;
                let global_bottom = global_center_y + cam_bottom;

                global_bottom - 50.
            });

            let any_dead = (&jumpers, &transforms, &velocities).join()
                .filter(|(_, _, v)| v.y < 0.)
                .any(|(_, t, _v)| {
                    //let v = v.y;
                    let j = t.translation().y;
                    let b = *death_threshold_y;
                    //info!("entity: {} threshold: {} velocity: {}", j, b, v);
                    j < b
                });

            if any_dead {
                return Trans::Switch(Box::new(GameOverState::new()));
            }

            Trans::None
        })
    }
}

impl<'a, 'b> InGameState<'a, 'b> {
    pub fn create_dispatcher(&mut self, w: &mut World) -> Dispatcher<'a, 'b> {
        let mut builder = DispatcherBuilder::new();

        builder.add(MovementSystemDesc.build(w), "movement_system", &[]);
        builder.add(FollowMouseSystemDesc.build(w), "follow_mouse_system", &["movement_system"]);

        builder.add(JumperCollisionSystemDesc.build(w), "jumper_collision_system", &[]);
        builder.add(PlatformPlacerSystemDesc::from_config(self.platform_placer_config.clone()).build(w),
                    "platform_placer_system", &[]);
        builder.add(CameraScrollerSystemDesc.build(w), "camera_scroller_system", &[]);

        let mut disp = builder
            .with_pool((*w.read_resource::<ArcThreadPool>()).clone())
            .build();
        disp.setup(w);
        disp
    }

    pub fn position_camera(&mut self, w: &mut World) {
        w.exec(|(mut transforms, cameras, screen): (WriteStorage<Transform>, ReadStorage<Camera>, ReadExpect<ScreenDimensions>)| {
            let (transform, _) = (&mut transforms, &cameras).join().exactly_one().map_err(|_| ()).unwrap();
            *transform = Default::default();
            transform.set_translation_xyz(screen.width() / 2., 300., 1.);
        });
    }

    pub fn create_base_platform(&mut self, w: &mut World) {
        let width = w.read_resource::<ScreenDimensions>().width();
        w.create_entity()
            .with(BoundingBox {
                min: Vector2::new(-width /2., -20.),
                max: Vector2::new(width/2., 0.),
            })
            .with({
                let mut t = Transform::default();
                t.set_translation_xyz(width / 2., 0., 0.);
                t
            })
            .build();
    }

    pub fn create_jumper(&mut self, w: &mut World) {
        w.create_entity()
            .with(self.prefab_jumper.clone())
            .build();
    }

    fn create_score_text(&self, world: &mut World) -> Entity {
        let prefab = (*world.read_resource::<TextPrefabs>()).in_game_score.clone();
        world
            .create_entity()
            .with(prefab)
            .build()
    }
}

#[derive(new)]
pub struct Score {
    #[new(default)]
    pub height: f32,
    pub text: Option<Entity>,
}

impl Score {
    pub fn score(&self) -> u32 {
        (self.height as i32).max(0) as u32
    }

    pub fn score_text(&self) -> String {
        format!("{}", self.score())
    }

    pub fn update_score(&mut self, height: f32, mut texts: WriteStorage<UiText>, templates: ReadStorage<UiTextTemplate>) {
        self.height = height;
        if let Some(entity) = self.text {
            if let Some(text) = texts.get_mut(entity) {
                let score = self.score_text();

                let formatted = if let Some(template) = templates.get(entity) {
                    SimpleCurlyFormat.format(&*template.template, &[score]).unwrap().into_owned()
                } else {
                    score
                };

                text.text = formatted;
            }
        }
    }
}
