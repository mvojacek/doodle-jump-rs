use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    audio::{output::Output, Source},
    assets::AssetStorage,
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage, Write},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3, Point3},
    ui::UiText,
};

use log::info;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use amethyst::core::timing::Time;
use crate::component::{Velocity2, Momentum2, Jumper, UiTextTemplate};
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};
use crate::state::in_game::Score;
use crate::unwrap_or_return;
use itertools::Itertools;

const CAMERA_SCORE_DIFFERENCE: f32 = 100.;

#[derive(SystemDesc, Default)]
#[system_desc(name(CameraScrollerSystemDesc))]
pub struct CameraScrollerSystem;

impl<'a> System<'a> for CameraScrollerSystem {
    type SystemData = (
        Option<Write<'a, Score>>,
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Jumper>,
        ReadStorage<'a, Camera>,
        WriteStorage<'a, UiText>,
        ReadStorage<'a, UiTextTemplate>,
    );

    fn run(&mut self, (score, mut transforms, jumpers, cameras, texts, templates): Self::SystemData) {
        let mut score = unwrap_or_return!(score);

        let jumper_y = (&jumpers, &transforms).join()
            .map(|(_, t)| t.translation().y)
            .fold1(|a, b| if a > b { a } else { b });
        let jumper_y = unwrap_or_return!(jumper_y);

        if score.height < jumper_y {
            score.update_score(jumper_y, texts, templates);
            // info!("score: {}", jumper_y);

            let (_, transform) = (&cameras, &mut transforms).join().next().unwrap();
            transform.translation_mut().y = jumper_y + CAMERA_SCORE_DIFFERENCE;
        }
    }
}