use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    audio::{output::Output, Source},
    assets::AssetStorage,
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage, WriteExpect},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3, Point3, Point2},
};

use log::info;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use amethyst::core::timing::Time;
use crate::component::{Velocity2, Momentum2, Jumper, BoundingBox, Platform};
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};
use itertools::Itertools;
use amethyst::renderer::debug_drawing::DebugLines;

#[derive(SystemDesc, Default)]
#[system_desc(name(JumperCollisionSystemDesc))]
pub struct JumperCollisionSystem;

impl<'a> System<'a> for JumperCollisionSystem {
    type SystemData = (
        WriteStorage<'a, Velocity2>,
        ReadStorage<'a, Transform>,
        ReadStorage<'a, Jumper>,
        ReadStorage<'a, Platform>,
        ReadStorage<'a, BoundingBox>,
        WriteExpect<'a, DebugLines>,
    );

    fn run(&mut self, (mut velocities, transforms, jumpers, _platforms, bbs, mut debug): Self::SystemData) {
        let global_collision_bbs: Lazy<Vec<BoundingBox>, _> = Lazy::new(|| {
            (&bbs, &transforms, !&jumpers).join().map(|(bb, transform, _)| {
                bb.transform(transform.global_matrix())
            }).collect_vec()
        });

        #[cfg(feature = "debug_boxes")]
            global_collision_bbs.iter().for_each(|bb| {
            debug.draw_rectangle(Point2::new(bb.min.x, bb.min.y),
                                 Point2::new(bb.max.x, bb.max.y),
                                 0.5, palette::Srgba::new(1.0f32, 0., 0., 1.));
        });

        for (jumper, velocity, transform, bb) in (&jumpers, &mut velocities, &transforms, &bbs).join() {
            if velocity.y >= 0. {
                continue;
            }

            let global_bb = bb.transform(transform.global_matrix());

            #[cfg(feature = "debug_boxes")]
                debug.draw_rectangle(Point2::new(global_bb.min.x, global_bb.min.y),
                                     Point2::new(global_bb.max.x, global_bb.max.y),
                                     0.5, palette::Srgba::new(0.0f32, 0., 1., 1.));

            let landed = global_collision_bbs.iter().any(|bb| bb.intersects(&global_bb));

            if landed {
                velocity.y = jumper.jump_strength;
            }
        }
    }
}