use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    audio::{output::Output, Source},
    assets::AssetStorage,
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3, Point3},
};

use log::info;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use amethyst::core::timing::Time;
use crate::component::{Velocity2, Momentum2};
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};

#[derive(SystemDesc, Default)]
#[system_desc(name(MovementSystemDesc))]
pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
    type SystemData = (
        ReadStorage<'a, Momentum2>,
        WriteStorage<'a, Velocity2>,
        WriteStorage<'a, Transform>,
        ReadExpect<'a, Time>,
    );

    fn run(&mut self, (momentums, mut velocities, mut transforms, time): Self::SystemData) {
        let mul = time.delta_seconds();
        for (velocity, transform) in (&velocities, &mut transforms).join() {
            //info!("Applying velocity({}, {}) to transform({}, {}) at scale {}", velocity.x, velocity.y, transform.translation().x, transform.translation().y, mul);
            velocity.apply(transform, mul);
        }
        for (momentum, velocity) in (&momentums, &mut velocities).join() {
            // info!("Applying mometum({}, {}) to velocity({}, {}) at scale {}", momentum.x, momentum.y, velocity.x, velocity.y, mul);
            momentum.apply(velocity, mul);
        }
    }
}