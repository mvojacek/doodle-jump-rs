use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    audio::{output::Output, Source},
    assets::{AssetStorage, Handle, Prefab},
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage, Entity, Entities, WriteExpect},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3, Point3, Point2},
};

use log::info;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use amethyst::core::timing::Time;
use crate::component::{Velocity2, Momentum2, Platform, BoundingBox};
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};
use amethyst::prelude::SystemDesc;
use std::path::Path;
use crate::config::PlatformPlacerConfig;
use crate::prefab::PlatformPrefab;
use rand::Rng;
use crate::unwrap_or_return;
use amethyst::renderer::debug_drawing::DebugLines;

pub struct PlatformPlacerSystemDesc {
    config: PlatformPlacerConfig,
}

impl PlatformPlacerSystemDesc {
    #[allow(dead_code)]
    pub fn from_config_path(path: impl AsRef<Path>) -> Self {
        PlatformPlacerSystemDesc::from_config(PlatformPlacerConfig::load(path))
    }
    pub fn from_config(config: PlatformPlacerConfig) -> Self {
        PlatformPlacerSystemDesc {
            config
        }
    }
}

impl SystemDesc<'_, '_, PlatformPlacerSystem> for PlatformPlacerSystemDesc {
    fn build(self, world: &mut World) -> PlatformPlacerSystem {
        let mut sys = PlatformPlacerSystem {
            config: self.config,
            ..Default::default()
        };
        sys.setup(world);
        sys
    }
}

#[derive(Default)]
pub struct PlatformPlacerSystem {
    config: PlatformPlacerConfig,
    recycled: Vec<Entity>,
    placed_up_to: f32,
    last_placed_y: f32,
    recycled_up_to: f32,
}

impl<'a> System<'a> for PlatformPlacerSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Platform>,
        Option<Read<'a, Handle<Prefab<PlatformPrefab>>>>,
        WriteStorage<'a, Handle<Prefab<PlatformPrefab>>>,
        ReadStorage<'a, Camera>,
        Entities<'a>,
        WriteExpect<'a, DebugLines>,
    );

    fn run(&mut self, (mut transforms, platforms, prefab, mut prefab_storage, cameras, entities, mut debug): Self::SystemData) {
        let prefab = unwrap_or_return!(prefab);

        let (cam_transform, cam) = unwrap_or_return!((&transforms, &cameras).join().next());

        let proj = cam.projection().as_orthographic().unwrap();
        let (cam_top, cam_bottom, cam_left, cam_right) = (proj.top(), proj.bottom(), proj.left(), proj.right());
        let &[global_center_x, global_center_y, _]: &[_; 3] = cam_transform.translation().as_ref();
        let global_top = global_center_y + cam_top + self.config.pregenerate;
        let global_bottom = global_center_y + cam_bottom - self.config.recycle_after;

        #[cfg(feature = "debug_boxes")]
            {
                let global_left = global_center_x + cam_left + self.config.border_x;
                let global_right = global_center_x + cam_right - self.config.border_x;

                debug.draw_rectangle(Point2::new(global_left, global_bottom),
                                     Point2::new(global_right, global_top),
                                     0.5, palette::Srgba::new(0., 1., 0., 1.))
            }


        for (transform, _, entity) in (&transforms, &platforms, &entities).join() {
            let y = transform.translation().y;
            if y < global_bottom && y >= self.recycled_up_to {
                self.recycled.push(entity)
            }
        }
        self.recycled_up_to = global_bottom;

        if global_top > self.placed_up_to {
            let global_left = global_center_x + cam_left + self.config.border_x;
            let global_right = global_center_x + cam_right - self.config.border_x;

            let mut rng = rand::thread_rng();
            let (step_min, step_max) = (self.config.step_min, self.config.step_max);

            self.last_placed_y = itertools::iterate(
                self.last_placed_y,
                move |&y| y + rng.gen_range(step_min, step_max),
            )
                .skip(1)
                .take_while(|&y| y < global_top)
                .inspect(|y| {
                    info!("new platform at: {}", y);
                })
                .map(move |y| (y, rng.gen_range(global_left, global_right)))
                .map(|(y, x)| {
                    let entity = self.get_unused_platform(&entities, &*prefab, &mut prefab_storage);
                    let mut t = Transform::default();
                    t.set_translation_xyz(x, y, 0.);
                    transforms.insert(entity, t).unwrap();
                    y
                }).last().unwrap_or(self.last_placed_y);

            self.placed_up_to = global_top
        }
    }
}

impl PlatformPlacerSystem {
    pub fn get_unused_platform(&mut self, e: &Entities,
                               prefab: &Handle<Prefab<PlatformPrefab>>,
                               storage: &mut WriteStorage<Handle<Prefab<PlatformPrefab>>>,
    ) -> Entity {
        self.recycled.pop().unwrap_or_else(|| {
            e.build_entity()
                .with((*prefab).clone(), storage)
                .build()
        })
    }
}