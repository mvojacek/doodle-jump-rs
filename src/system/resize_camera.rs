use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    assets::AssetStorage,
    audio::{output::Output, Source},
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
};

use log::info;

#[derive(SystemDesc)]
#[system_desc(name(ResizeCameraSystemDesc))]
pub struct ResizeCameraSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<WEvent>,
}

impl ResizeCameraSystem {
    fn new(reader_id: ReaderId<WEvent>) -> Self {
        Self { reader_id }
    }
}

impl<'a> System<'a> for ResizeCameraSystem {
    type SystemData = (
        Read<'a, EventChannel<WEvent>>,
        WriteStorage<'a, Camera>,
        WriteStorage<'a, Transform>,
    );

    fn run(&mut self, (events, mut cameras, mut transforms): Self::SystemData) {
        for event in events.read(&mut self.reader_id) {
            if let WEvent::WindowEvent { window_id: _, event: WindowEvent::Resized(dims) } = event {
                for (camera, transform) in (&mut cameras, &mut transforms).join() {
                    let (w, h) = (dims.width as f32, dims.height as f32);
                    *camera = Camera::standard_2d(w, h);
                    transform.set_translation_xyz(w * 0.5, h * 0.5, 1.);
                    //info!("resized camera to {}x{}", w, h);
                }
            }
        }
    }
}