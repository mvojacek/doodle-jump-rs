use amethyst::prelude::*;
use amethyst::winit::Event as WEvent;
use amethyst::winit::WindowEvent;
use amethyst::{
    assets::AssetStorage,
    audio::{output::Output, Source},
    core::transform::Transform,
    derive::SystemDesc,
    ecs::prelude::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
    core::shrev::{EventChannel, ReaderId},
    renderer::Camera,
    window::ScreenDimensions,
    core::math::{Vector3,Point3},
};

use log::info;
use crate::component::FollowMouse;
use once_cell::unsync::Lazy;
use std::ops::Deref;
use crate::util::{ToVector4, IntoVector3, global_to_transform_local, screen_to_world_global};

#[derive(SystemDesc)]
#[system_desc(name(FollowMouseSystemDesc))]
pub struct FollowMouseSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<WEvent>,
}

impl FollowMouseSystem {
    fn new(reader_id: ReaderId<WEvent>) -> Self {
        Self { reader_id }
    }
}

impl<'a> System<'a> for FollowMouseSystem {
    type SystemData = (
        Read<'a, EventChannel<WEvent>>,
        ReadStorage<'a, FollowMouse>,
        WriteStorage<'a, Transform>,
        ReadExpect<'a, ScreenDimensions>,
    );

    fn run(&mut self, (events, followers, mut transforms, screen): Self::SystemData) {
        for event in events.read(&mut self.reader_id) {
            if let WEvent::WindowEvent {
                window_id: _, event: WindowEvent::CursorMoved {
                    device_id: _, modifiers: _,
                    position: pos,
                }
            } = event {
                for (follower, transform) in (&followers, &mut transforms).join() {
                    if follower.x {
                        transform.set_translation_x(pos.x as f32 * screen.hidpi_factor() as f32);
                    }
                    if follower.y {
                        transform.set_translation_y(-pos.y as f32 * screen.hidpi_factor() as f32);
                    }
                }
            }
        }
    }
}