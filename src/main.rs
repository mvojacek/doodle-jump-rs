#![allow(unused_imports)]
#![recursion_limit = "10000"]

#![feature(type_alias_impl_trait)]

#[macro_use]
extern crate derivative;
#[macro_use]
extern crate derive_new;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate getset;
#[macro_use]
extern crate amethyst;
#[macro_use]
extern crate amethyst_inspector;
#[macro_use]
extern crate shrinkwraprs;

use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow, RenderDebugLines},
        types::DefaultBackend,
        RenderingBundle,
    },
    utils::application_root_dir,
    LoggerConfig, Logger,
    ui::{UiBundle, RenderUi},
    input::StringBindings,
};
use log::LevelFilter;
use amethyst_imgui::RenderImgui;
use amethyst::input::InputBundle;
use crate::prefab::{JumperPrefab, TextPrefab};
use crate::inspector::InspectorBundle;
use crate::prefab::text::UiTransformPrefabComp;

mod state;
mod config;
mod component;
mod bundle;
mod system;
#[macro_use]
mod util;
mod prefab;
mod inspector;

fn main() -> amethyst::Result<()> {
    // use amethyst::core::Named;
    // use amethyst::ui::Anchor;
    // let prefab = TextPrefab {
    //     name: Named::new("name"),
    //     text: Default::default(),
    //     transform: UiTransformPrefabComp {
    //         id: "test".to_string(),
    //         anchor: Anchor::TopLeft,
    //         pivot: Anchor::TopLeft,
    //         translation: [1., 2., 3.],
    //         width: 1.0,
    //         height: 2.0
    //     },
    //     template: Some(Default::default()),
    // };
    // println!("{}", ron::ser::to_string_pretty(&prefab, ron::ser::PrettyConfig::default()).unwrap());
    // return Ok(());

    //panic!();

    Logger::from_config(LoggerConfig::default())
        .level_for("gfx_backend_vulkan", LevelFilter::Off)
        .start();

    let app_root = application_root_dir()?;

    let resources = app_root.join("resources");
    let display_config = resources.join("display_config.ron");
    let platform_placer_config = resources.join("platform_placer_config.ron");



    let game_data = {
        let mut game_data = GameDataBuilder::default()
            .with_bundle(TransformBundle::new())?
            .with_bundle(InputBundle::<StringBindings>::default())?
            .with_bundle({
                let bundle = RenderingBundle::<DefaultBackend>::new()
                    .with_plugin(
                        RenderToWindow::from_config_path(display_config)
                            .with_clear([0.34, 0.36, 0.52, 1.0]),
                    )
                    .with_plugin(RenderFlat2D::default())
                    .with_plugin(RenderImgui::<StringBindings>::default())
                    .with_plugin(RenderUi::default());
                if cfg!(feature = "debug_boxes") {
                    bundle.with_plugin(RenderDebugLines::default())
                } else {
                    bundle
                }
            }
            )?
            .with_bundle(UiBundle::<StringBindings>::new())?
            .with_bundle(bundle::BaseBundle::<StringBindings>::default())?;
        if cfg!(feature = "inspector") {
            game_data = game_data.with_bundle(InspectorBundle)?;
        }
        game_data
    };

    let mut game = Application::new(resources, state::InitialState::new(platform_placer_config), game_data)?;
    game.run();

    Ok(())
}
