use amethyst::core::components::Transform;
use amethyst::core::math::{Point3,Vector3,Vector4};
use amethyst::core::math::geometry::Isometry3;
use amethyst::window::ScreenDimensions;
use amethyst::renderer::Camera;

#[allow(dead_code)]
pub fn screen_to_world_global(x: f32, y: f32, camera: &Camera, camera_transform: &Transform, screen: &ScreenDimensions) -> Vector3<f32> {
    camera.projection().screen_to_world_point(
        Point3::new(x, y, 0.) * screen.hidpi_factor() as f32,
        screen.diagonal(),
        camera_transform)
        .coords
}

#[allow(dead_code)]
pub fn global_to_transform_local(global: &Vector4<f32>, transform: &Transform) -> Vector4<f32> {
    transform.view_matrix() * (transform.global_matrix() * global)
}
