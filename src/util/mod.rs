use amethyst_inspector::InspectControl;
use amethyst::prelude::World;
use amethyst::assets::Handle;
use amethyst::assets::Prefab;
use amethyst::prelude::*;
use amethyst::shred::Read;
use amethyst::assets::AssetStorage;
use serde_derive::{Serialize, Deserialize};
use amethyst::assets::PrefabData;
use amethyst::ecs::EntityBuilder;
use amethyst::core::Axis2;
use amethyst::assets::ProgressCounter;

pub mod graphics;
#[macro_use]
pub mod macros;
pub mod multi_progress_counter;

pub use graphics::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Direction2 {
    XPos,
    XNeg,
    YPos,
    YNeg,
}

impl Direction2 {
    pub fn axis(self) -> Axis2 {
        use Direction2::*;
        use Axis2::*;
        match self {
            XPos | XNeg => X,
            YPos | YNeg => Y,
        }
    }
}

use amethyst_inspector::debug_inspect_control;

debug_inspect_control![
    Direction2
];

use amethyst::core::math::{Scalar, Vector2, Vector3, Vector4, U1, U2};
use amethyst::core::alga::general::{Identity, Operator, Additive, Multiplicative};
use std::fmt::Debug;

pub trait ToVector4<N: Scalar> {
    fn to_vec4(&self) -> Vector4<N>;
}

impl<N: Scalar> ToVector4<N> for Vector2<N>
    where N: Identity<Additive> + Identity<Multiplicative> {
    fn to_vec4(&self) -> Vector4<N> {
        Vector4::new(self.x, self.y, <N as Identity<Additive>>::identity(), <N as Identity::<Multiplicative>>::identity())
    }
}

impl<N: Scalar> ToVector4<N> for Vector3<N>
    where N: Identity<Multiplicative> {
    fn to_vec4(&self) -> Vector4<N> {
        Vector4::new(self.x, self.y, self.z, <N as Identity::<Multiplicative>>::identity())
    }
}

pub trait ToVector3<N: Scalar> {
    fn to_vec3(&self) -> Vector3<N>;
}

impl<N: Scalar> ToVector3<N> for Vector2<N>
where N: Identity<Additive> {
    fn to_vec3(&self) -> Vector3<N> {
        Vector3::new(self.x, self.y, <N as Identity<Additive>>::identity())
    }
}

pub trait IntoVector2<N: Scalar> {
    fn into_vec2(self) -> Vector2<N>;
}

impl<N: Scalar> IntoVector2<N> for Vector3<N> {
    fn into_vec2(self) -> Vector2<N> {
        self.remove_fixed_rows::<U1>(2)
    }
}

impl<N: Scalar> IntoVector2<N> for Vector4<N> {
    fn into_vec2(self) -> Vector2<N> {
        self.remove_fixed_rows::<U2>(2)
    }
}

pub trait IntoVector3<N: Scalar> {
    fn into_vec3(self) -> Vector3<N>;
}

impl<N: Scalar> IntoVector3<N> for Vector4<N> {
    fn into_vec3(self) -> Vector3<N> {
        self.remove_fixed_rows::<U1>(3)
    }
}

