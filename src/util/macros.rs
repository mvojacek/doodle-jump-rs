#[macro_export]
macro_rules! unwrap_or_return {
    ($e:expr) => {
        match $e {
            Some(v) => v,
            None => return,
        }
    };
    ($e: expr, return $err:expr) => {
        match $e {
            Some(v) => v,
            None => return $err,
        }
    }
}