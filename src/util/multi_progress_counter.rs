use amethyst_inspector::InspectControl;
use amethyst::prelude::World;
use amethyst::assets::Handle;
use amethyst::assets::Prefab;
use amethyst::prelude::*;
use amethyst::shred::Read;
use amethyst::assets::AssetStorage;
use serde_derive::{Serialize, Deserialize};
use amethyst::assets::PrefabData;
use amethyst::ecs::EntityBuilder;
use amethyst::core::Axis2;
use amethyst::assets::ProgressCounter;
use std::sync::atomic::Ordering;
use itertools::Itertools;
use amethyst::assets::Completion;
use serde::export::fmt::Debug;

#[derive(Default, new)]
pub struct MultiProgressCounter {
    #[new(default)]
    vec: Vec<ProgressCounter>,
    locked: bool,
}

#[allow(dead_code)]
impl MultiProgressCounter {
    pub fn next(&mut self) -> &mut ProgressCounter {
        self.vec.push(ProgressCounter::default());
        self.vec.last_mut().unwrap()
    }

    pub fn lock(&mut self) {
        self.locked = true;
    }

    pub fn unlock(&mut self) {
        self.locked = false;
    }
}

pub type AssetErrorMetaAlias = impl Debug;

#[allow(dead_code)]
impl MultiProgressCounter {
    /// Removes all errors and returns them.
    pub fn errors(&self) -> Vec<AssetErrorMetaAlias> {
        self.vec.iter().flat_map(|p| p.errors().into_iter()).collect_vec()
    }

    /// Returns the number of assets this struct is tracking.
    pub fn num_assets(&self) -> usize {
        self.vec.iter().map(|p| p.num_assets()).sum()
    }

    /// Returns the number of assets that have failed.
    pub fn num_failed(&self) -> usize {
        self.vec.iter().map(|p| p.num_failed()).sum()
    }

    /// Returns the number of assets that are still loading.
    pub fn num_loading(&self) -> usize {
        self.vec.iter().map(|p| p.num_loading()).sum()
    }

    /// Returns the number of assets that have successfully loaded.
    pub fn num_finished(&self) -> usize {
        self.vec.iter().map(|p| p.num_finished()).sum()
    }

    /// Returns `Completion::Complete` if all tracked assets are finished.
    pub fn complete(&self) -> Completion {
        if self.locked {
            return Completion::Loading;
        }
        for comp in self.vec.iter().map(|p| p.complete()) {
            return match comp {
                Completion::Complete => continue,
                s => s,
            };
        }

        Completion::Complete
    }

    pub fn is_complete(&self) -> bool {
        self.complete() == Completion::Complete
    }
}
