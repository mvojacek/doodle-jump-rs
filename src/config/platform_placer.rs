use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize, Derivative)]
#[derivative(Default)]
#[serde(default)]
pub struct PlatformPlacerConfig {
    #[derivative(Default(value="200."))]
    pub step_max: f32,
    #[derivative(Default(value="150."))]
    pub step_min: f32,
    #[derivative(Default(value="81."))]
    pub border_x: f32,
    #[derivative(Default(value="100."))]
    pub pregenerate: f32,
    #[derivative(Default(value="30."))]
    pub recycle_after: f32,
}

