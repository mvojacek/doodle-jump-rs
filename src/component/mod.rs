use amethyst::ecs::prelude::*;
use amethyst_inspector::Inspect;
use amethyst::ecs::Component;
use amethyst::assets::PrefabData;
use amethyst::Error;

pub mod follow_mouse;
pub mod jumper;
pub mod platform;
pub mod bounding_box;
pub mod velocity;
pub mod momentum;

pub use follow_mouse::FollowMouse;
pub use jumper::Jumper;
pub use platform::Platform;
pub use bounding_box::BoundingBox;
pub use velocity::Velocity2;
pub use momentum::Momentum2;
use std::borrow::Cow;

#[derive(Clone, Component, Debug, PrefabData, Serialize, Deserialize, Shrinkwrap, Derivative)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
#[shrinkwrap(mutable)]
#[serde(transparent)]
#[serde(default)]
#[derivative(Default)]
pub struct UiTextTemplate {
    #[derivative(Default(value = "Cow::Borrowed(\"{}\")"))]
    pub template: Cow<'static, str>
}
