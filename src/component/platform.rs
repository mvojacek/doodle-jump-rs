use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize,Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;

#[derive(Clone, Default, Component, Debug, Deserialize, Serialize, PrefabData, Inspect)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
//#[inspect(no_default)]
pub struct Platform {
}
