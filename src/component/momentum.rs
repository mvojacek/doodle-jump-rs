use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::VecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize,Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use itertools::Itertools;
use crate::component::Velocity2;
use std::ops::{DerefMut, Deref};

#[derive(Clone, Component, Derivative, Inspect, Debug, PrefabData, Serialize, Deserialize)]
#[prefab(Component)]
#[derivative(Default)]
#[storage(VecStorage)]
#[inspect(no_default)]
#[serde(default)]
pub struct Momentum2 {
    #[derivative(Default(value="Vector2::new(0.,0.)"))]
    pub m: Vector2<f32>,
}

impl Momentum2 {
    pub fn apply(&self, v: &mut Velocity2, mul: f32) {
        **v += **self * mul
    }
}

impl Deref for Momentum2 {
    type Target = Vector2<f32>;

    fn deref(&self) -> &Self::Target {
        &self.m
    }
}