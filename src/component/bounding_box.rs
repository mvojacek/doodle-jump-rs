use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use itertools::Itertools;
use amethyst::core::math::base::Matrix4;

#[derive(Clone, Component, Inspect, Debug, PrefabData, Serialize, Deserialize)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
#[inspect(no_default)]
pub struct BoundingBox {
    pub min: Vector2<f32>,
    pub max: Vector2<f32>,
}

impl BoundingBox {
    pub fn normalized(a: Vector2<f32>, b: Vector2<f32>) -> Self {
        let ([ax, ay], [bx, by]): ([_; 2], [_; 2]) = (a.into(), b.into());
        let (min_x, max_x) = if ax > bx { (bx, ax) } else { (ax, bx) };
        let (min_y, max_y) = if ay > by { (by, ay) } else { (ay, by) };
        BoundingBox {
            min: Vector2::new(min_x, min_y),
            max: Vector2::new(max_x, max_y),
        }
    }

    pub fn transform(&self, mat: &Matrix4<f32>) -> Self {
        use crate::util::{ToVector4, IntoVector2};

        Self::normalized((mat * self.min.to_vec4()).into_vec2(),
                         (mat * self.max.to_vec4()).into_vec2())
    }

    pub fn contains(&self, x: f32, y: f32) -> bool {
        x >= self.min.x && x <= self.max.x && y >= self.min.y && y <= self.max.y
    }

    pub fn intersects(&self, bb: &Self) -> bool {
        let &[ax, ay]: &[_; 2] = bb.min.as_ref();
        let &[bx, by]: &[_; 2] = bb.max.as_ref();

        [(ax, ay), (ax, by), (bx, ay), (bx, by)].iter().any(|&(x, y)| self.contains(x, y))
    }
}