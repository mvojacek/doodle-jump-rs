use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::VecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize,Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use amethyst::core::Transform;
use std::ops::{Deref, DerefMut};
use crate::util::ToVector3;

#[derive(Clone, Component, Derivative, Inspect, Debug, PrefabData, Serialize, Deserialize)]
#[prefab(Component)]
#[derivative(Default)]
#[storage(VecStorage)]
#[inspect(no_default)]
#[serde(default)]
pub struct Velocity2 {
    #[derivative(Default(value="Vector2::new(0.,0.)"))]
    v: Vector2<f32>,
}

impl Velocity2 {
    pub fn apply(&self, t: &mut Transform, mul: f32) {
        t.prepend_translation(self.to_vec3() * mul);
    }
}

impl Deref for Velocity2 {
    type Target = Vector2<f32>;

    fn deref(&self) -> &Self::Target {
        &self.v
    }
}

impl DerefMut for Velocity2 {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.v
    }
}