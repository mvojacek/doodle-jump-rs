use amethyst::ecs::Component;
use amethyst::assets::Handle;
use amethyst::ecs::storage::DenseVecStorage;
use amethyst_inspector::Inspect;
use amethyst::ecs::world::WorldExt;
use amethyst::core::math::base::Vector2;
use amethyst::derive::PrefabData;
use amethyst::assets::PrefabData;
use serde_derive::{Serialize, Deserialize};
use amethyst::Error;
use amethyst::ecs::prelude::*;
use derive_new::new;
use itertools::Itertools;
use amethyst::core::math::base::Matrix4;

#[derive(Debug, Clone, Copy, Component, Derivative, Serialize, Deserialize, PrefabData)]
#[derivative(Default)]
#[prefab(Component)]
#[storage(DenseVecStorage)]
#[serde(default)]
pub struct FollowMouse {
    #[derivative(Default(value="true"))]
    pub x: bool,
    #[derivative(Default(value="true"))]
    pub y: bool,
}
